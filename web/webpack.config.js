import { resolve } from "path";

export const entry = "./src/index.tsx";
export const devtool = "inline-source-map";

export const module = {
  rules: [
    {
      test: /\.bin$/,
      type: 'asset/inline',
      use: [
        {
          loader: 'url-loader',
          options: {
            encoding: false,
            mimetype: false,
            generator: (content) => {
              return content;
            }
          }
        }
      ]
    }
  ]
};

export const output = {
  filename: "bundle.js",
  path: resolve(__dirname, "build/static")
};
