main:
	; init lcd
	mov lcdc, 0x1 				; clear display
	mov lcdc, 0xF 				; display on, cursor on, blinking on
	mov lcdc, 0x38 				; function set: 8 bit words, 2 lines

	mov cb, [txt]				; cb becomes pointer to txt
	mov a, 0				; mov 0 to a for comparison with the current character

printStr:
	load d, [cb] 				; load the current character
	inc b 					; move pointer to next character

	cmp a, d 				; check if current char is NULL
	jz halt 				; if so we reached the end of the string

	mov lcd, d 				; otherwise just print the character
	jmp printStr 				; continue the loop

halt:
	hlt

txt: #d "Hello, world!\0"
