// highly simplified: https://github.com/hlorenzi/customasm/blob/f4e04293ad3de2d2c24624e48ec9314a157f17a1/web/main.js
import * as customasm from 'customasm';
import CBT_cpu from './CBT.cpu';

export async function assembleBinary(code: string): Promise<Uint8Array> {
    // put the CPU definition at the top of the code
    const file = await fetch(CBT_cpu);
    const cpuDefinition = await file.text();
    let input = cpuDefinition + code;

    try {
        return customasm.safe_wasm_assemble("binary", input);
    }
    catch (e) {
        let output = e as string;
        console.log(output);
        output = output.replace(/\</g, "&lt;")
        output = output.replace(/\>/g, "&gt;")
        output = output.replace(/\n/g, "<br>")
        output = output.replace(/\x1b\[90m/g, "</span><span style='color:gray;'>")
        output = output.replace(/\x1b\[91m/g, "</span><span style='color:red;'>")
        output = output.replace(/\x1b\[93m/g, "</span><span style='color:#f80;'>")
        output = output.replace(/\x1b\[96m/g, "</span><span style='color:#08f;'>")
        output = output.replace(/\x1b\[97m/g, "</span><span style='color:black;'>")
        output = output.replace(/\x1b\[1m/g, "</span><span style='font-weight:bold;'>")
        output = output.replace(/\x1b\[0m/g, "</span><span style='color:black;'>")

        output = "<span style='color:black;'>" + output + "</span>"

        throw output;
    }
}
