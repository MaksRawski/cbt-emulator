import { Tooltip } from "@mui/material";
import { CPUModule } from "./Modules";

interface InterruptFLags{
    inm: boolean,
    irq: boolean
}

export class Interrupts extends CPUModule<{}, InterruptFLags>{
    name = "PIC"
    description = "Programmable Interrupts Controller"

    constructor(props: any){
        super(props);
        this.state = {inm: false, irq: false};
    }
    componentDidMount(): void {
        // global.set...
    }
    module(){
        const OFF = '○';
        const ON = '●';

        return (
            <div className="flags" id="interrupt-flags">
                <div className="LED">
                    {this.state.inm ? ON : OFF}
                    {this.state.irq ? ON : OFF}
                </div>
                <div className="flags-label">
                    <Tooltip placement="top" title="Interrupt Mask"><span>INM </span></Tooltip>
                    <Tooltip placement="top" title="Interrupt Request"><span>IRQ</span></Tooltip>
                </div>
            </div>
        );
    }
}
