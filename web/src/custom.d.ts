declare module '*.bin' {
    const content: any;
    export default content;
}

declare module '*.cpu' {
    const content: string;
    export default content;
}
