export const cbt_asm = {
	'comment': /;.*/,
	'string': /\".*\"/,
	'pointer': {
		pattern: /\[w+\]/,
		alias: 'punctuation'
	},
	'section': /((\.?\w+:)|(?<=(jmp|jc|jh|jo|jz))\s+\.?\w+)/,
	'op-code': {
		pattern: /\s+(?:nop|ha?lt|mov|jmp|jc|jh|jo|jz|load|pop|ret|store|push|call)/,
		alias: 'keyword'
	},
	'alu-op': {
		pattern: /(?:not|nor|nand|xor|xnor|and|or|add|adc|sub|sbc|cmp|inc|dec|dbl|shl)/,
		alias: 'keyword'
	},
	'hex-number': {
		pattern: /0x[\da-f]{1,2}/i,
		alias: 'number'
	},
	'binary-number': {
		pattern: /0b[01]{1,8}/i,
		alias: 'number'
	},
	'decimal-number': {
		pattern: /\d{1,3}/,
		alias: 'number'
	},
	'special-register': {
		pattern: /(?:sp|pc|lcd|lcdc)/,
		alias: 'register'
	},
	'general-purpose-register': {
		pattern: /r?[a-d]/i,
		alias: 'register'
	},
	'reg-pair':{
		pattern: /(?:dc|cb|ba|da)/,
		alias: 'register'
	},
};
