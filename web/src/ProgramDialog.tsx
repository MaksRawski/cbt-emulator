/* import CodeEditor from '@uiw/react-textarea-code-editor'; */
import Editor from 'react-simple-code-editor';
import { Button, FormControl, IconButton, InputLabel, MenuItem, Modal, Select, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React, { ReactNode } from "react";
import { cpu, hello_world } from './cpu';

import SaveIcon from '@mui/icons-material/Save';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';

import './css/syntax.css';
import Prism from 'prismjs';
import { cbt_asm } from './cbtAsmDefinition';

import init from 'customasm';
import { assembleBinary } from './customasm';

interface state {
    compiled: boolean,
    saved: boolean,
}

interface LocalStorage {
    programs: Program[],
}

interface Program {
    name: string,
    code: string,
}

enum ProgramType {
    Example = "example_",
    User = "user_",
    Other = "other_",
}

interface ModalState {
    open: boolean,
    currentProgram: { name: string, code: string, bin?: Uint8Array, programType: ProgramType, state: state }
}

const examplePrograms = ["hello world"];

export class ProgramSelector extends React.Component<{}, ModalState> {
    constructor(props: any) {
        super(props);

        this.state = {
            open: false, currentProgram:
            {
                name: "hello world", code: "...", bin: hello_world,
                state: { compiled: true, saved: true }, programType: ProgramType.Example
            }
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.updateCode = this.updateCode.bind(this);
        this.modal = this.modal.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.setupLocalStorage();
    }

    componentDidMount(): void {
        this.loadExampleProgram("hello world");
        init().then(_ => console.log("customasm loaded succesfully!"));
        document.addEventListener('keydown', this.handleKeyDown);
    }
    loadExampleProgram(programName: string) {
        let programFile = programName.replace(" ", "_") + ".asm";
        fetch(`${process.env.PUBLIC_URL}/programs/${programFile}`).then(res => {
            if (!res.ok) {
                throw new Error(`Failed to load file ${programFile}! Response: ${res}`);
            };

            res.text().then(code => {
                this.setState({
                    currentProgram: {
                        name: programName, code,
                        state: { compiled: false, saved: true }, programType: ProgramType.Example
                    }
                });
            });
        });
    }
    setupLocalStorage(): void {
        if (localStorage.getItem("programs") === null) {
            let default_storage: LocalStorage = {
                programs: [{
                    name: "hello world",
                    code: "asdf movie",
                }]
            };
            localStorage.setItem("programs", JSON.stringify(default_storage));
        }
    }
    button(): ReactNode {
        return (
            <Button
                variant="outlined"
                sx={{ color: "white", fontSize: "0.5em" }}
                onClick={this.openModal}>
                Select a program
            </Button>
        )
    }
    openModal(): void {
        this.setState({ open: true });
    }
    closeModal(): void {
        this.setState({ open: false });
    }
    updateCode(code: string): void {
        this.setState({ currentProgram: { ...this.state.currentProgram, code, bin: undefined, state: { saved: false, compiled: false } } });
    }
    compile(program: string): void {
        assembleBinary(program).then(res => {
            document.getElementById("logs")!.innerHTML = "Compiled succesfully, loading into memory...";
            cpu.load_program(res);
            document.getElementById("logs")!.innerHTML += `<br/>Loaded ${cpu.view_rom().length} bytes!`;
        }).catch(e => {
            (document.getElementById("logs") as HTMLSpanElement).innerHTML = e as string;
        });
    }
    getSavedPrograms(): Program[] {
        let storage = localStorage.getItem("programs");
        if (storage === null) throw new Error("Local storage not initialized yet!");
        let parsedStorage: LocalStorage = JSON.parse(storage);
        return parsedStorage["programs"];
    }
    getSavedProgramsNames(): string[] {
        return this.getSavedPrograms().map((p: Program) => p.name);
    }
    saveCurrentProgram(): void {
        let name = this.state.currentProgram.name;
        let code = this.state.currentProgram.code;

        let programs = this.getSavedPrograms();
        let updatedPrograms;
        if (programs.find(p => p.name === name)) {
            updatedPrograms = programs.map((p) => p.name === name ? { name, code } : p);
        } else {
            programs.push({ name, code });
            updatedPrograms = programs;
        }

        let updatedStorage: LocalStorage = { programs: updatedPrograms };
        localStorage.setItem("programs", JSON.stringify(updatedStorage));

        this.setState({
            currentProgram: {
                ...this.state.currentProgram,
                state: { ...this.state.currentProgram.state, saved: true },
                programType: ProgramType.User
            }
        });
    }
    handleKeyDown(event: KeyboardEvent): void {
        if (event.ctrlKey && event.key === 's') {
            event.preventDefault();
            this.saveCurrentProgram();
        }
    }
    loadProgramFromStorage(name: string): void {
        let program = this.getSavedPrograms().find(p => p.name === name);
        if (program === null) throw new Error(`No such program ${name} found in local storage!`);
        let code = program!.code;
        this.setState({
            currentProgram: {
                name, code, state:
                    { saved: true, compiled: false }, programType: ProgramType.User
            }
        })
    }
    clearLogs(): void{
        (document.getElementById("logs") as HTMLSpanElement).innerHTML = "";
    }
    newProgram(): void {
        this.setState({
            currentProgram: {
                name: "", code: "",
                state: { saved: false, compiled: false }, programType: ProgramType.User
            }
        });
        this.clearLogs();
    }
    loadProgram(name: string): void {
        if (name.startsWith("example_")) {
            let programName = name.slice("example_".length, name.length);
            this.loadExampleProgram(programName);
        } else if (name.startsWith("user_")) {
            let programName = name.slice("user_".length, name.length);
            this.loadProgramFromStorage(programName);
        } else {
            console.error("There is no such program as " + name);
            return;
        }
        this.clearLogs();
    }
    programSelector(): ReactNode {
        let programs = this.getSavedProgramsNames();
        return (
            <FormControl variant="filled" style={{ flexGrow: 3 }}>
                <InputLabel>Saved programs</InputLabel>
                <Select
                    value={this.state.currentProgram.programType + this.state.currentProgram.name}
                    onChange={(e) => this.loadProgram(e.target.value)}>
                    <MenuItem value="" disabled><em>Example programs</em></MenuItem>
                    {examplePrograms.map((name, index) => (<MenuItem key={index} value={"example_" + name}>{name}</MenuItem>))}

                    <MenuItem value="" disabled><em>User programs</em></MenuItem>
                    {programs.map((name, index) => (<MenuItem key={index} value={"user_" + name}>{name}</MenuItem>))}
                </Select>
            </FormControl>
        );
    }
    deleteProgram(name: string) {
        let programs = this.getSavedPrograms();
        let newPrograms: Program[] = programs.filter((p) => p.name !== name);
        let newStorage: LocalStorage = { programs: newPrograms };

        localStorage.setItem("programs", JSON.stringify(newStorage))
        if (newPrograms.length === 0) {
            this.loadExampleProgram(examplePrograms[0]);
        } else {
            this.loadProgramFromStorage(newPrograms[0].name);
        }
    }
    editorInterface(): ReactNode {
        return (
            <Box sx={{ display: "flex", gap: "0.4em", marginBottom: "0.4em" }}>
                <TextField
                    variant="outlined"
                    placeholder="program name"
                    value={this.state.currentProgram.name}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                        {/* if (event.target.value in this.getSavedProgramsNames()) {
                            // TODO: show soft error
                        } */}
                        this.setState({ currentProgram: { ...this.state.currentProgram, name: event.target.value } });
                    }}
                    error={this.state.currentProgram.name === ""}
                    style={{ display: (!this.state.currentProgram.state.saved ? "block" : "none") }}
                />
                <Button variant="contained" onClick={() => this.saveCurrentProgram()}
                    disabled={this.state.currentProgram.state.saved || this.state.currentProgram.name === ""}
                    sx={{ display: (!this.state.currentProgram.state.saved ? "block" : "none") }} >
                    <SaveIcon sx={{ fontSize: "1.6em", display: "block" }} />
                </Button>
                {this.programSelector()}
                <IconButton onClick={() => this.newProgram()} aria-label="Create new program">
                    <AddIcon />
                </IconButton>
                <IconButton disabled={this.state.currentProgram.programType !== ProgramType.User} onClick={() => this.deleteProgram(this.state.currentProgram.name)}>
                    <DeleteIcon />
                </IconButton>
                {/* disabled={this.state.currentProgram.state.compiled} */}
                <Button variant="contained"
                    disabled={this.state.currentProgram.state.compiled}
                    onClick={() => this.compile(this.state.currentProgram.code)}>
                    Compile
                </Button>
            </Box>
        )
    }

    modal(): ReactNode {
        const modalStyle = {
            color: "white",
            bgcolor: "#050005",
            width: "80vw",
            height: "80vh",
            border: "5px solid white",
            borderRadius: "5px",

            position: "absolute",
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            p: 2
        }
        return (
            <Modal open={this.state.open} onClose={this.closeModal}>
                {/* <Modal open={true} onClose={this.closeModal}> */}
                <Box sx={modalStyle}>
                    {this.editorInterface()}
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/firacode@6.2.0/distr/fira_code.css" />
                    <Box sx={{
                        display: "flex", maxHeight: "75vh", width: "100%", flexDirection: "column",
                        justifyContent: "space-between"
                    }}>
                        <div style={{ maxHeight: "70%", width: "90%", overflow: "auto", flex: "1 1 auto" }}>
                            <Editor
                                value={this.state.currentProgram.code}
                                onValueChange={this.updateCode}
                                highlight={code => Prism.highlight(code, cbt_asm, '')}
                                padding={10}
                                style={{ fontFamily: "'Fira Code', monospace", backgroundColor: "#111", borderRadius: "0.5em" }} />
                        </div>
                        <span id="logs" style={{
                            width: "90%", display: "block", background: "white", color: "black",
                            flex: "0 0 20%", maxHeight: "30%", overflow: "auto"
                        }}></span>
                    </Box>
                </Box>
            </Modal>
        );

    }

    render(): ReactNode {
        return (
            <Box sx={{ position: "absolute", right: "5vw" }}>
                {this.button()}
                {this.modal()}
            </Box>
        )
    }
}
