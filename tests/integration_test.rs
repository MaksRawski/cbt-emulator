// https://rustwasm.github.io/wasm-bindgen/wasm-bindgen-test/usage.html

use cbt_emulator::cpu::Cpu;

#[cfg(test)]
mod test_instructions {
    use super::*;
    #[test]
    fn test_mov() {
        let mut cpu = Cpu::new();
        // mov a, 42
        cpu.load_program(vec![0b00_000_111, 42]);

        // mov takes 6 steps
        for _ in 0..6 {
            cpu.tick();
        }
        assert_eq!(cpu.ra.data, 42)
    }
    #[test]
    fn test_jumps() {
        let mut cpu = Cpu::new();

        // jmp test
        // mov a, 2
        // hlt
        //
        // test:
        //  mov a, 42
        //  hlt
        cpu.load_program(vec![
            0b00101111, 0b00000000, 0b00000110, 0b00000111, 0b00000010, 0b00110110, 0b00000111,
            0b00101010, 0b00110110,
        ]);
        for _ in 0..50 {
            cpu.tick();
        }
        assert_eq!(cpu.ra.data, 42);
    }

    #[test]
    fn test_conditional_jumps() {
        let mut cpu = Cpu::new();
        // main:
        //   mov a, 1
        //   dec a
        //   jz zero
        //   mov b, 255
        //   hlt
        // zero:
        //   mov b, 42

        cpu.load_program(vec![
            0x07, 0x01, 0xf8, 0x2b, 0x00, 0x09, 0x0f, 0xff, 0x36, 0x0f, 0x2a,
        ]);
        for _ in 0..40 {
            cpu.tick();
        }
        assert_eq!(cpu.rb.data, 42);
    }

    #[test]
    fn test_loads() {
        let mut cpu = Cpu::new();

        // mov b, 4
        // load a, [cb]
        // hlt
        //
        // #d8 42
        cpu.load_program(vec![
            0b00001111, 0b00000100, 0b01000001, 0b00110110, 0b00101010,
        ]);
        for _ in 0..40 {
            cpu.tick();
        }
        assert_eq!(cpu.ra.data, 42);
    }
}
#[cfg(test)]
mod test_lcd {
    use super::*;

    #[test]
    fn test_lcd_out() {
        let mut cpu = Cpu::new();

        // mov lcd, 42
        // mov lcdc, 0xf // display on, cursor on, blining on
        cpu.load_program(vec![0b00_110_111, 42, 0b00_111_110, 0xf]);

        for _ in 0..7 {
            cpu.tick();
        }
        // display is off so there shouldn't be any value
        assert_eq!(cpu.lcd.content(), None);

        // turning on the display
        for _ in 0..7 {
            cpu.tick();
        }
        assert_eq!(cpu.lcd.content().unwrap()[0], 42);
    }
    #[test]
    fn test_lcd_string() {
        let mut cpu = Cpu::new();

        // mov lcdc, 0x1     ; clear display
        // mov lcdc, 0xF     ; display on, cursor on, blinking on
        // mov lcdc, 0x38    ; function set: 8 bit words, 2 lines
        //
        // mov lcd, 65
        // mov lcd, 66
        // mov lcd, 67
        cpu.load_program(vec![
            0x3e, 0x01, 0x3e, 0x0f, 0x3e, 0x38, 0x37, 0x41, 0x37, 0x42, 0x37, 0x43,
        ]);

        for _ in 0..50 {
            cpu.tick();
        }
        assert_eq!(cpu.lcd.content().unwrap()[0..3], [65, 66, 67]);
    }
}
#[cfg(test)]
mod test_programs {
    use super::*;
    // #[wasm_bindgen_test]
    #[test]
    fn test_hello_world() {
        let mut cpu = Cpu::new();
        // outp | addr | data
        //  0:0 |    0 |          ; main:
        //  0:0 |    0 | 27 ff    ; mov SP, 0xFF
        //  2:0 |    2 | 3e 01    ; mov lcdc, 0x1
        //  4:0 |    4 | 3e 0f    ; mov lcdc, 0xF
        //  6:0 |    6 | 3e 38    ; mov lcdc, 0x38
        //  8:0 |    8 | 39 00 18 ; mov cb, [txt]
        //  b:0 |    b | 07 00    ; mov a, 0
        //  d:0 |    d |          ; printStr:
        //  d:0 |    d | 59       ; load d,[cb]
        //  e:0 |    e | f5       ; inc b
        //  f:0 |    f | f3       ; cmp a,d
        // 10:0 |   10 | 2b 00 17 ; jz halt
        // 13:0 |   13 | 33       ; mov lcd,d
        // 14:0 |   14 | 2f 00 0d ; jmp printStr
        // 17:0 |   17 |          ; halt:
        // 17:0 |   17 | 36       ; hlt
        // 18:0 |   18 |          ; txt:
        // 18:0 |   18 | 48 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21 00 ; #d "Hello, world!\0"
        cpu.load_program(include_bytes!("hello_world.bin").to_vec());

        for _ in 0..600 {
            cpu.tick();
        }

        assert_eq!(
            cpu.lcd.string_content(),
            Some(format!("Hello, world!{}", "\0".repeat(32 - 13)))
        );
    }
    // #[test]
    fn test_interrupts() {
        let mut cpu = Cpu::new();
        //   outp | addr | data
        //    0:0 |    0 |          ; main:
        //    0:0 |    0 | 27 ff    ; mov SP, 0xFF
        //    2:0 |    2 | 3e 0c    ; mov lcdc,0xC
        //    4:0 |    4 | 07 30    ; mov a, 48
        //    6:0 |    6 |          ; .loop:
        //    6:0 |    6 | 00       ; nop
        //    7:0 |    7 | 2f 00 06 ; jmp .loop
        // 7000:0 | 7000 |          ; interrupt:
        // 7000:0 | 7000 | 3e 10    ; mov lcdc, 0x10
        // 7002:0 | 7002 | f4       ; inc a
        // 7003:0 | 7003 | 30       ; mov lcd, a
        // 7004:0 | 7004 | 6c       ; ret
        cpu.load_program(include_bytes!("interrupts.bin").to_vec());

        // go to the nop
        for _ in 0..21 {
            cpu.tick();
        }

        cpu.request_interrupt(1);

        // interrupt's microcode
        for _ in 0..16 {
            cpu.tick();
        }

        // interrupt's subroutine
        for _ in 0..18 {
            cpu.tick();
        }
        // IRQ still set
        assert_eq!(cpu.pic.irq, true);
        // and (apropriate device from) the queue reset
        assert_eq!(cpu.pic.queue, 0);

        let mut expected_result = vec![0; 32];
        expected_result[0] = 49;
        assert_eq!(cpu.lcd.content(), Some(expected_result));

        // ret
        for _ in 0..14 {
            cpu.tick();
        }
        dbg!(&cpu.pic);

        dbg!(cpu.mem.ram.0[0x7FFF]);

        // INM should be restored and IRQ reset
        assert_eq!(cpu.pic.inm, true);
        assert_eq!(cpu.pic.irq, false);
    }
}
