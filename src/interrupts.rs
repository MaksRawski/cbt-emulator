use std::fmt::Debug;

pub struct ProgrammableInterruptController {
    /// inm - interrupt mask, true means interrupts are enabled
    pub inm: bool,
    /// irq - Interrupt request, true if there is a pending interrupt
    pub irq: bool,

    /// value of the nth-bit from the right represents
    /// whether the device with that id has requested an interrupt
    pub queue: u8,
}

impl Debug for ProgrammableInterruptController {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("{\n")?;
        f.write_fmt(format_args!("\tINM: {}\n", self.inm))?;
        f.write_fmt(format_args!("\tIRQ: {}\n", self.irq))?;
        f.write_fmt(format_args!("\tqueue: {:08b}\n", self.queue))?;
        f.write_str("}")?;

        Ok(())
    }
}

impl ProgrammableInterruptController {
    pub fn new() -> Self {
        Self {
            inm: false,
            irq: false,
            queue: 0,
        }
    }

    pub fn set_inm(&mut self) {
        self.inm = true;
    }

    pub fn reset_inm(&mut self) {
        self.inm = false;
    }

    pub fn request(&mut self, device_id: u8) {
        self.irq = true;
        self.queue |= device_id;
    }

    /// similar to Flags.to_byte()
    pub fn flags_out(&self) -> u8 {
        self.inm as u8 | (self.irq as u8) << 1
    }

    pub fn flags_in(&mut self, flags: u8) {
        self.inm = flags & 1 > 0;
        self.inm = flags & 2 > 0;
    }

    /// returns HPC for the jump address for the highest priority device (one with the lowest id)
    /// and marks that device as handled.
    ///
    /// | IO device | jump address |
    /// |-----------+--------------|
    /// | device 0  |       0x7000 |
    /// | device 1  |       0x7200 |
    /// | device 2  |       0x7400 |
    /// | device 3  |       0x7600 |
    /// | device 4  |       0x7800 |
    /// | device 5  |       0x7A00 |
    /// | device 6  |       0x7C00 |
    /// | device 7  |       0x7E00 |
    ///
    /// e.g. for the queue = 0b0100_1010
    /// 1. handle() -> 0x72
    /// 2. handle() -> 0x76
    /// 3. handle() -> 0x7C
    ///
    pub fn handle(&mut self) -> u8 {
        // .trailing_zeros returns u32 for some reason but
        // since the input number is u8 it's safe to unwrap
        // also as it's an assembly instruction in both x86 (bsf) and wasm (ctz) it's incredibly fast
        let device_id: u8 = self.queue.trailing_zeros().try_into().unwrap();

        // mark the device as handled
        self.queue ^= 1 << device_id;
        self.irq = false;

        return 0x7 << 4 | device_id << 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_flags_io() {
        let mut pic = ProgrammableInterruptController::new();

        assert_eq!(pic.flags_out(), 0);

        pic.set_inm();
        assert_eq!(pic.flags_out(), 1);

        pic.request(0);
        assert_eq!(pic.flags_out(), 3);

        pic.reset_inm();
        assert_eq!(pic.flags_out(), 2);
    }
}
